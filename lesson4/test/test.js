'use strict';
// logic
describe('min(x, y)', function () {
    describe('Возвращает наименьшее число из двух. Реализация через тернарный оператор "?"', function () {
        it('2 и 3 == 2', function () {
            assert.equal(min(2, 3), 2);
        });
        it('35 и 197 == 35', function () {
            assert.equal(min(35, 197), 35);
        });
        it('100 и 99 == 99', function () {
            assert.equal(min(100, 99), 99);
        });
        it('50 и 50 == false', function () {
            assert.equal(min(50, 50), false);
        });
        it('gaga и biber == biber', function () {
            assert.equal(min("gaga", "biber"), "biber");
        });
        it('null и 52 == null', function () {
            assert.equal(min(null, 52), null);
        });
        it('undefined и 52 == false', function () {
            assert.equal(min(undefined, 52), false);
        });
    });
});

describe('minif(x, y)', function () {
    describe('Возвращает наименьшее число из двух. Реализация через "if"', function () {
        it('2 и 3 == 2', function () {
            assert.equal(minif(2, 3), 2);
        });
        it('35 и 197 == 35', function () {
            assert.equal(minif(35, 197), 35);
        });
        it('100 и 99 == 99', function () {
            assert.equal(minif(100, 99), 99);
        });
        it('50 и 50 == false', function () {
            assert.equal(minif(50, 50), false);
        });
        it('gaga и biber == biber', function () {
            assert.equal(minif("gaga", "biber"), "biber");
        });
        it('null и 52 == null', function () {
            assert.equal(minif(null, 52), null);
        });
        it('undefined и 52 == false', function () {
            assert.equal(minif(undefined, 52), false);
        });
    });
});

describe('countBs(string)', function () {
    describe('Возвращает количество символов "B" в строке', function () {
        it('BLOB == 2', function () {
            assert.equal(countBs('BLOB'), 2);
        });
        it('"NaN" == 0', function () {
            assert.equal(countBs('NaN'), 0);
        });
        it('NaN == false', function () {
            assert.equal(countBs(NaN), false);
        });
    });
});

describe('countChar(string, char)', function () {
    describe('Возвращает количество символов char в строке string', function () {
        it('BLOB, B == 2', function () {
            assert.equal(countChar('BLOB', 'B'), 2);
        });
        it('"NaN", a == 1', function () {
            assert.equal(countChar('NaN', 'a'), 1);
        });
        it('NaN, any == false', function () {
            assert.equal(countChar(NaN), false);
        });
        it('155, 5 == 2', function () {
            assert.equal(countChar(155, 5), 2);
        });
    });
});

describe('isEven(number)', function () {
    describe('Возвращает boolean четности числа при условии Boolean(0) == false, Boolean(1) == true', function () {
        it("30 == true", function () {
            assert.equal(isEven(30), false);
        });
        it("75 == false", function () {
            assert.equal(isEven(75), true);
        });
        it("'-1' == true", function () {
            assert.equal(isEven("-1"), true);
        });
        it("null == null", function () {
            expect(isEven(null)).to.be.null;
        });
        it("undefined == null", function () {
            expect(isEven(undefined)).to.be.null;
        });
        it("NaN == null", function () {
            expect(isEven(NaN)).to.be.null;
        });
        it("'string1' == null", function () {
            expect(isEven('string1')).to.be.null;
        });
    });
});

// actions
describe('addTag(string, object)', function () {
    describe('Создать элемент и вернуть его', function () {
        it("a.tagName == 'A'", function () {
            expect(addTag('a')).to.have.property('tagName', 'a'.toUpperCase());
        });
        it("a.href == http://google.ru/index.html; a.id == 'link'", function () {
            expect(addTag('a', {
                "href": "http://google.ru/index.html",
                "id": "link"
            })).to.have.property("href", "http://google.ru/index.html");
            expect(addTag('a', {
                "href": "http://google.ru/index.html",
                "id": "link"
            })).to.have.property("id", "link");
        });
    });
});
