'use strict';

/////////////
// Common //
///////////

function addTag(tagName, attrs) {
    var elem = document.createElement(tagName);
    if (!attrs) {
        return elem;
    } else if (typeof (attrs) !== 'object') {
        return elem;
    } else {
        for (var attr in attrs) {
            elem.setAttribute(attr, attrs[attr]);
        }
    };
    return elem;
};

//function justShow(element) {
//    var showme = document.getElementById(element);
//    if (showme.style.display == "block") {
//        showme.style.display = "none";
//    } else if (showme.style.display == "none" || showme.style.display == "") {
//        showme.style.display = "block";
//    }
//};


////////////
// Task1 //
//////////

// Constructor
//
var myTask_1 = document.getElementById('task1'),
    input1_1 = addTag("input", {
        "class": "field",
        "value": 0
    }),
    input1_2 = addTag("input", {
        "class": "field",
        "value": 0
    }),
    result1 = addTag("p", {
        "style": "display: block;",
        "class": "result"
    }),
    showButton1 = addTag("button", {
        "onclick": "getMin(input1_1, input1_2)"
    }),
    desc1_1 = document.createElement("p"),
    desc1_2 = document.createElement("p");


desc1_1.innerHTML = "Введите первое число или фразу";
desc1_2.innerHTML = "Введите второе число или фразу";
showButton1.innerHTML = "Показать результат";
myTask_1.appendChild(input1_1);
myTask_1.appendChild(input1_2);
myTask_1.appendChild(showButton1);
myTask_1.appendChild(result1);
result1.innerHTML = 'результат будет тут';
myTask_1.insertBefore(desc1_1, input1_1);
myTask_1.insertBefore(desc1_2, input1_2);
//
// endConstructor

// Logic
//

// получить в элемент результат
function getMin(first, second) {
    return result1.innerHTML = min(first.value, second.value) || "не могу выбрать наименьшее";
};

function min(x, y) {
    return (x < y) ? x :
        (x > y) ? y : false;
};
//
// endLogic


////////////
// Task2 //
//////////

// Constructor
//
var myTask_2 = document.getElementById('task2'),
    input2_1 = addTag("input", {
        "class": "field",
        "value": 0
    }),
    input2_2 = addTag("input", {
        "class": "field",
        "value": 0,
        "maxlength": 1
    }),
    result2 = addTag("p", {
        "style": "display: block;",
        "class": "result"

    }),
    showButton2 = addTag("button", {
        "onclick": "getCountChar(input2_1, input2_2)"
    }),
    desc2_1 = document.createElement("p"),
    desc2_2 = document.createElement("p");


desc2_1.innerHTML = "Введите слово, фразу или число";
desc2_2.innerHTML = "Введите искомый символ";
showButton2.innerHTML = "Показать результат";
myTask_2.appendChild(input2_1);
myTask_2.appendChild(input2_2);
myTask_2.appendChild(showButton2);
myTask_2.appendChild(result2);
result2.innerHTML = 'результат будет тут';
myTask_2.insertBefore(desc2_1, input2_1);
myTask_2.insertBefore(desc2_2, input2_2);
//
// endConstructor

// Logic
//

//Найти кол-во char в строке string
function countChar(string, char) {
    function realString(check) {
        if (check) {
            return check.toString().toLowerCase();
        } else {
            //            console.log('Задание 2 == Вы ввели не строку. Выход.');
            return false;
        };
    };
    //выходим, если вернулось false
    if (!realString(string) || !realString(char)) return false;
    var charCount = 0;
    for (let i of realString(string)) {
        if (i == realString(char)) {
            charCount += 1;
            //            console.log(charCount);
        };
    };

    return charCount;
};

// получить в элемент результат
function getCountChar(string, char) {
    return result2.innerHTML = countChar(string.value, char.value) || "0 или неверные данные";
}
//
// endLogic


////////////
// Task3 //
//////////

// Constructor
//
var myTask_3 = document.getElementById('task3'),
    input3 = addTag("input", {
        "class": "field",
        "value": 0
    }),
    result3 = addTag("p", {
        "style": "display: block;",
        "class": "result"

    }),
    showButton3 = addTag("button", {
        "onclick": "getIsEven(input3)"
    }),
    desc3 = document.createElement("p");


desc3.innerHTML = "Введите число";
showButton3.innerHTML = "Показать результат";
myTask_3.appendChild(input3);
myTask_3.appendChild(showButton3);
myTask_3.appendChild(result3);
result3.innerHTML = 'результат будет тут';
myTask_3.insertBefore(desc3, input3);
//
// endConstructor

// Logic
//

function isEven(number) {
    if (isNaN(parseInt(number))) return null;
    number = Math.round(Math.abs(number));
    if (number === 0) {
        return false;
    } else if (number === 1) {
        return true;
    }
    return isEven(number - 2);
};

function getIsEven(num) {
    var evenResult = isEven(num.value);
    if (evenResult === null) {
        return result3.innerHTML = "Неверно введены данные";
    } else if (evenResult) {
        return result3.innerHTML = "Число НЕчетное";

    } else if (!evenResult) {
        return result3.innerHTML = "Число четное";

    };
};
//
// endLogic
