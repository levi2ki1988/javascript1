'use strict';


////////////////
// задание 2 //
//////////////
var oldArray = [1, 2, 3, 4, 29, [5, [17, "jane"], 22], 91, "cyber"],
    revArray = [1, 2, 3, 4, 29, [5, [17, "jane"], 22], 91, "cyber"];

// возвращает перевернутый массив
function reverseArray(inArray) {
    var outArray = [];
    // перебирает входящий массив с конца, исходящий с начала
    for (var i = inArray.length - 1, n = 0; i >= 0; i--, n++) {
        outArray[n] = inArray[i];
    };
    return outArray;
};

// возвращает перевернутый массив, обходя его рекурсивно
function reverseArrayRecursive(inArray) {
    var outArray = [];
    // перебирает входящий массив с конца, исходящий с начала
    // проверяет данные на соостветствие типу Array и возвращает перевернутый массив, если true
    for (var i = inArray.length - 1, n = 0; i >= 0; i--, n++) {
        if (Array.isArray(inArray[i])) {
            outArray[n] = reverseArrayRecursive(inArray[i]);
        } else {
            outArray[n] = inArray[i];
        }
    };
    return outArray;
};

// переворачивает массив на лету
function reverseArrayInPlace(inArray) {
    console.time('vavaka');
    for (var i = inArray.length; i > 0; i--) {
        inArray.push(inArray.splice(i - 1, 1)[0]);
    };
    console.timeEnd('vavaka');
};

////////////////
// задание 1 //
//////////////
var t2Array = [1, 2, 3];
var outputList = arrayToList(t2Array);
console.log("outputList \n", JSON.stringify(outputList, false, 4))
var t2NewArray = listToArray(outputList);
console.log("____________________________\n");
console.log("t2NewArray \n", t2NewArray);
//

// массив в список
function arrayToList(array) {
    var list = {};
    var context = list;

    function chList(value, rest) {
        if (this.rest === undefined) {
            this.value = value;
            this.rest = rest;
            return context = this.rest;
        }
    };
    for (var i in array) {
        if (array.length - 1 == i) {
            chList.call(context, array[i], null);
        } else {
            chList.call(context, array[i], {});
        }
    };
    return list;
}

// список в массив
function listToArray(list) {
    var newArray = [];
    var context = list;

    function chList() {
        if (this.rest !== undefined) {
            newArray.push(this.value);
            return context = this.rest;
        }
    };
    while (context) {
        chList.call(context);
    };
    return newArray;
}

// добавить элемент списка в начало
function prepend(newValue, list) {
    var copy = Object.assign({}, list);
    var a = new Object({
        "value": newValue,
        "rest": copy
    });
    return a;
}

// поиск в массиве по номеру ( начиная с 1, не с 0)
function findnth(itemNum, list) {
    var context = list;
    var i = 0;
    while (i < itemNum - 1) {
        if (!context) return undefined;
        findItem.call(context, list);
        i++;
    }

    function findItem(list) {
        return context = this.rest || undefined;
    }
    return context;
}

////////////////
// задание 3 //
//////////////

var user = {
        name: 'Sergey',
        age: 30,
        email: 'sergey@gmail.com',
        friends: ['Sveta', 'Artem']
    },
    anotherUser = {
        name: 'Поросёнок Пётр',
        car: 'Трактор',
        address: 'Рашка'
    },
    pickKeys = ['name', 'email', 'address', 'friends', 'something', 'something else', 'and another thing'];

function pick(obj, keys) {
    var newObj = {};
    keys.forEach(function (value, index, array) {
        if (obj[value] !== undefined) {
            newObj[value] = obj[value];
        }
    });
    return newObj;
}

Array.prototype.reverseArrayInPlace = function(){
    console.time('vaka');
    for(var i = 0, el; i < this.length/2 - 1; i++){ // this.length/2 - 1 == 1.5, i == 0, el
        el = this[this.length - 1 - i]; // el == this[5 - 1 - 0] === this[4]
        this[this.length - 1 - i] = this[i]; // this[4] == this[0]
        this[i] = el; // this[0] == this[4]
    } // i == 1
    console.timeEnd('vaka');
}

var mmm = new Array(1,2,3,4,5);
var mmm2 = new Array(1,2,3,4,5);
//mmm.reverseArrayInPlace();
//reverseArrayInPlace(mmm2);