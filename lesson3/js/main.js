'use strict';

// функции для контента
function justshow(element) {
    var showme = document.getElementById(element);
    if (showme.style.display == "block") {
        showme.style.display = "none";
    } else if (showme.style.display == "none" || showme.style.display == "") {
        showme.style.display = "block";
    }
};

/*
 * Задание 1. Написать треугольник из решеток. Запросить у пользователя кол-во строк.
 */
console.log('--- Задание 1 ---');
var tripple = "";
for (var total = parseInt(prompt('Введите количество строк треугольника', 4)), n = 1, char = "\u2b1b"; total >= n; n++, char += "\u2b1b") {
    tripple += char + "\n";
};
console.log(tripple);

// добавляем результат в html файл, заменяя ньюлайн на тег <br />
var trippledom = document.createElement("p");
trippledom.innerHTML = tripple.replace(/\n/g, "<br />");
var divugol = document.getElementById("ugol");
divugol.appendChild(trippledom);


// Задание2 с шахматной доской
console.log('--- Задание 2 ---');

// Задаем размер через prompt, что бы можно было менять размер доски
var width = 8,  // длинна доски
    height = 8; // высота доски
//жаль что для этого придется всё переделать - при нечетном числе получается не шахматный порядок, возможно можно было сделать срезами вместо генератора
//while (width % 2 != 0) width = parseInt(prompt("Введите длинну доски", 8));
//while (height % 2 != 0) height = parseInt(prompt("Введите высоту доски", 8)); 

    //Бесконечный генератор последовательности знаков
    function* my_chees() {
        while (true) {
            yield "\u2b1b";
            yield "\u2b1c";
        };
    };

    //Бесконечный генератор последовательности значений
    function* heightchess(cheesArray) {
        var arr = cheesArray,
            rev = cheesArray.slice().reverse();
        while (true) {
            yield arr;
            yield rev;
        }
    };

    //Генерация массива из генератора последовательности с заданным в задаче значением
    function checkmate(generator, count) {
        var arr = [];
        for (var i = 0; i < count; ++i) {
            arr.push(generator.next().value);
        }
        return arr;
    };

    // пустой массив для сбора значений
    var boardFin = [];

    // вроде называется замыкание, инициаторы генераторов + callback функция в методе Array.prototype.forEach - передает значения в массив сбора.
    // сразу преобразует матрицу в промежуточный результат
    checkmate(
        heightchess(
            checkmate(my_chees(), width)
        ),
        height
    ).forEach(function (element) {
        boardFin.push(element.join(""));
    });

    // Используем метод join для объединения уровней матрицы в строку - в задании написано "создать программу, которая делает СТРОКУ"
    boardFin = boardFin.join("\n");
    console.log(boardFin);

    // добавляем результат в html файл, заменяя ньюлайн на тег <br />
    var srcrennchess = document.createElement("p");
    srcrennchess.innerHTML = boardFin.replace(/\n/g, "<br />");
    var divschess = document.getElementById("chess");
    divschess.appendChild(srcrennchess);
