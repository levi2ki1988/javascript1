'use strict';

var biber = {
    name: "Justin",
    last_name: "Biber",
    role: "singer",
    like: "puke"
};

function vavaka(tobj) {
    return JSON.stringify(tobj)
        .replace(/:/g, "=")
        .replace(/{|}|"/g, "")
        .replace(/,/g, "&");
}

function vavakaSuper(tobj) {
    return JSON.stringify(biber)
        .replace(/([^,a-z0-9_:])/gi, "")
        .replace(/:/g,"=")
        .split(',').join('&');
}