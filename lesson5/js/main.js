'use strict';

function openTask(taskName) {
    var i,
        x = document.getElementsByClassName("task"),
        y = document.getElementsByClassName("menu");
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }
    for (i = 0; i < y.length; i++) {
        y[i].className = "menu inactive";
    }
    document.getElementById(taskName).style.display = "block";
}

// Задание 1
var colorObjects = {
        red: document.getElementById("red"),
        green: document.getElementById("green"),
        blue: document.getElementById("blue")
    },
    colorValues = {
        red: "0",
        green: "0",
        blue: "0"
    },
    colorHex = "#000",
    hexObject = document.getElementById("hex");

function getHex() {
    var arr = [],
        prefix = "#";
    for (let i in colorValues) {
        arr.push(parseInt(colorObjects[i].value).toString(16));
    }
    arr.forEach(function (value, index, array) {
        array[index] = value.replace(/^(\w)$/g, "0$1");
    });
    return colorHex = prefix + arr.join("");
}

function getColorValue(id) {
    getHex();
    hexObject.value = colorHex.toUpperCase();
    document.getElementById("color-result").style.background = colorHex;
}



for (let i in colorObjects) {
    // не рекомендовано так делать
    colorObjects[i].addEventListener('input', function () {

        if (this.value >= 0 && this.value <= 255) {
            getColorValue(this.id);
            colorObjects[i].style.background = "white";
        } else {
            colorValues[i] = NaN;
            colorObjects[i].style.background = "red";
        }
    });
}

// Задание 2
var numberObject = document.getElementById("number");

numberObject.addEventListener('input', function () {
    var obj = {
            'единицы': "",
            'десятки': "",
            'сотни': ""
        },
        keys = Object.keys(obj);
    if (Math.abs(this.value) > 999) {
        this.style.background = "red";
        return console.log(obj = {}, "число больше 999 или меньше -999");
    } else {
        this.style.background = "white";
        var newValue = Math.abs(this.value).toString();
        if (newValue.length === 1) {
            newValue = "00" + newValue;
        } else if (newValue.length === 2) {
            newValue = "0" + newValue;
        }
        newValue.split("").reverse().forEach(function (value, index) {
            obj[keys[index]] = parseInt(value);
        });
    }
    return console.log(obj);
});

// Задание 3

function objectToQueryString(objec) {
    var queryArray = [];
    for (let key in objec) {
        queryArray.push(key + "=" + objec[key]);
    }
    return console.log(queryArray.join("&"));
}

var vasya = {
        user: "Vasya",
        id: 127,
        password: "karamultyk891"
    },
    biber = {
        name: "Justin",
        last_name: "Biber",
        role: "singer",
        like: "puke"
    };

// Задание 4

//массивы для проверки
var arrFof2 = ['21', '22'],
    arrFof3 = ["31", "32", "33"],
    arrFof4 = ['41', '42', '43', '44'],
    arrFof5 = ['51', '52', '53', '54', '55'];

// приватное пространство имен
function nameSpace() {
    var counterFof = 0,
        lastLength,
        fofer = function (FoF) {
            if (FoF.length === counterFof) counterFof = 0;
            if (lastLength > FoF.length) {
                counterFof = lastLength - (2 + lastLength - FoF.length); //и можно передать массив на сколько угодно меньше, правило соблюдётся
                }
                for (var i = 0; i < counterFof; i++) {
                    console.log(FoF[i]);
                }
                console.log(FoF.slice(-FoF.length + i, FoF.length).join("_"));
                //End
                lastLength = FoF.length;
                ++counterFof;
                return console.log("counterFof " + counterFof + '\n', "FoF " + FoF + '\n', "lastLength " + lastLength + '\n');
            }
            return fofer;
        }

    var makeMeSuperStar = new nameSpace();
