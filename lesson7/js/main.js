'use strict';

var body = document.body,
    boardSize = [1, 2, 3, 4, 5, 6, 7, 8], // поля цифр
    boardLetters = ["A", "B", "C", "D", "E", "F", "G", "H"], // поля букв
    cleanBlock = document.createElement("div"), //пустой блок
    // Наборы символов фигур
    figures = {
        white: {
            king: {
                fig: "\u2654",
                name: "Белый король",
                system: "white.king"
            },
            queen: {
                fig: "\u2655",
                name: "Белая королева",
                system: "white.queen"
            },
            rook: {
                fig: "\u2656",
                name: "Белая ладья",
                system: "white.rook"
            },
            bishop: {
                fig: "\u2657",
                name: "Белый офицер",
                system: "white.bishop"
            },
            knight: {
                fig: "\u2658",
                name: "Белый конь",
                system: "white.knight"
            },
            pawn: {
                fig: "\u2659",
                name: "Белая пешка",
                system: "white.pawn"
            }
        },
        black: {
            king: {
                fig: "\u265a",
                name: "Черный король",
                system: "black.king"

            },
            queen: {
                fig: "\u265b",
                name: "Черная королева",
                system: "black.queen"

            },
            rook: {
                fig: "\u265c",
                name: "Черная ладья",
                system: "black.rook"

            },
            bishop: {
                fig: "\u265d",
                name: "Черный офицер",
                system: "black.bishop"

            },
            knight: {
                fig: "\u265e",
                name: "Черный конь",
                system: "black.knight"

            },
            pawn: {
                fig: "\u265f",
                name: "Черная пешка",
                system: "black.pawn"

            }
        }
    };

// генератор пустой доски
function makeCleanGrid() {
    var grid = document.createDocumentFragment(), // ФРАГМЕНТ решетка доски
        line = document.createElement("div"), // линия с ячейками
        letters = document.createElement("div"), // линия с буквами
        numbers = document.createElement("div"), // ячейки с цифрами
        cell = document.createElement("div"); // ячейки доски

    // задаем нужные классы
    letters.setAttribute("class", "letter-grid");
    line.setAttribute("class", "line");
    cell.setAttribute("class", "cell");
    numbers.setAttribute("class", "clean-block number-block");

    // делаем блоки букв
    for (let i of boardLetters) {
        let currentBlock = cleanBlock.cloneNode(true);
        currentBlock.className += " letter-block";
        currentBlock.innerHTML = i;
        letters.appendChild(currentBlock);
    }
    // ставим пустые блоки в начало и в конец грида для выравнивания
    letters.insertAdjacentElement("afterbegin", cleanBlock.cloneNode(true));
    letters.insertAdjacentElement("beforeend", cleanBlock.cloneNode(true));
    letters.firstChild.className += " letter-block";
    letters.lastChild.className += " letter-block";
    var normalizer = letters.cloneNode(true);
    normalizer.firstChild.style.borderTopLeftRadius = "10px";
    normalizer.lastChild.style.borderTopRightRadius = "10px";
    // внедряем верхние буквы
    grid.appendChild(normalizer);

    // строим саму доску
    var idd = 1;
    for (let i = 0, bS = 0; i < boardSize.length; i++, bS++) {
        if (bS === boardSize.length) {
            bS = 0;
        }
        var currentLine = line.cloneNode(true), // текущая линия
            currentNumber = numbers.cloneNode(true); // текущий номер линии
        // выставляем атрибуты нодам
        currentLine.setAttribute("id", "line-" + boardSize[i]);
        currentNumber.innerHTML = boardSize[i];
        currentLine.appendChild(currentNumber.cloneNode(true));
        // генерим ячейки линии с помощью классов css
        for (let n = 0, bL = 0; n < boardSize.length; n++, bL++) {
            if (bL === boardLetters.length) {
                bL = 0;
            }
            var currentCell = cell.cloneNode(true);
            if (i % 2) {
                if (n % 2) {
                    currentCell.className = "cell black-block";
                } else {
                    currentCell.className = "cell white-block";
                }
            } else {
                if (n % 2) {
                    currentCell.className = "cell white-block";
                } else {
                    currentCell.className = "cell black-block";
                }
            }
            currentCell.setAttribute("id", "cell_" + idd);
            idd++
            currentCell.setAttribute("data-range", boardLetters[bL] + boardSize[bS]);
            currentCell.setAttribute("data-system", "");
            currentCell.setAttribute("title", "");
            // запрещаем выделение текста //
            currentCell.setAttribute("data-toggle", "");
            currentCell.onselectstart = function () {
                return false;
            };
            currentCell.unselectable = "on";
            currentCell.style.MozUserSelect = "none";
            currentCell.style.KhtmlUserSelect = "none";
            currentCell.style.webkitUserSelect = "none";
            //
            currentLine.appendChild(currentCell.cloneNode(true));
        }
        // сначала ячейки в линию, потом линию в грид
        currentLine.appendChild(currentNumber.cloneNode(true));
        grid.appendChild(currentLine);
    }

    // нижние буквы
    grid.appendChild(letters.cloneNode(true));
    return grid;
}

// --- Вызов этих функций надо делать с определенным контекстом --- \\
function genFirure() {
    return [this.rook, this.knight, this.bishop, this.queen, this.king, this.bishop, this.knight, this.rook];
}

function genPawn() {
    var array = new Array(8);
    return array.fill(this.pawn);
}
// --- --- \\

// эта функция принимает объект линию и сгенерированный масив фигур
function fillFigures(line, figures) {
    var cells = line.querySelectorAll(".cell");
    for (let i = 0; i < figures.length; i++, n += 400) {
        setTimeout(function () {
            moveHand();
            cells[i].innerHTML = figures[i].fig
            cells[i].setAttribute("data-system", figures[i].system)
            cells[i].setAttribute("title", figures[i].name)
        }, n);
    }
}

// первый опыт анимации с помощью JS, основан на разнице во времени от старта, симуляция 50 кадров в секунду
function moveHand() {
    hand.style.display = "block";
    var start = Date.now(); // сохранить время начала

    var timer = setInterval(function () {
        // вычислить сколько времени прошло из opts.duration
        var timePassed = Date.now() - start;
        var leftDelta = timePassed / 5;
        hand.style.left = (parseInt(rect.left - 70) + leftDelta) + 'px';

        if (timePassed > 250) {
            clearInterval(timer);
            hand.style.display = "none";
        }

    }, 20);
};


// попробуем разные способы взаимодействия с DOM
body.innerHTML = '<div id="hand"><img src="img/hand1.png" /></div>\n<div id="chessboard"></div>'; // добавляем картинку руку и главнй контейнер
cleanBlock.setAttribute("class", "clean-block"); // класс пустому блоку

// забираем в переменную главный контейнер и генерируем пустую доску
var chessboard = document.getElementById("chessboard");
chessboard.appendChild(makeCleanGrid());

// забираем строки доски, на которые будем фигуры ставить
var lines = {
        //line 1 Белые фигуры
        line1: document.getElementById("line-1"),
        //line 2 Белые пешки
        line2: document.getElementById("line-2"),
        //line 7 Черные пешки
        line7: document.getElementById("line-7"),
        //line 8 Черные фигуры
        line8: document.getElementById("line-8")
    },
    hand = document.getElementById("hand"), // забираем руку в переменную
    rect = chessboard.getBoundingClientRect(); // вычисляем координаты главного контйнера

// даем руке координаты и скрываем руку
hand.style.top = parseInt(rect.top) + "px";
hand.style.left = parseInt(rect.left - 70) + "px";
hand.style.display = "none";

// n это старт для таймеров
var n = 400;

// заполняем ячейки строк массивом фигур
fillFigures(lines.line1, genFirure.call(figures.white)); // белые фигуры
fillFigures(lines.line2, genPawn.call(figures.white)); // белые пешки
fillFigures(lines.line8, genFirure.call(figures.black)); // черные фигуры
fillFigures(lines.line7, genPawn.call(figures.black)); // черные пешки

// создаем заголовок
var h1 = document.createElement("h1");
h1.innerHTML = "Поиграй со мной";
h1.setAttribute("id", "h1");
document.body.insertAdjacentElement("afterbegin", h1);

//////////////////////////////////////////////////////////////////////////
var toggles = document.querySelectorAll(".cell");

function toggle() {
    toggles = document.querySelectorAll(".cell");
    toggles.forEach(function (value) {
        value.dataset.toggle = "";
    })
    this.dataset.toggle == true ? this.dataset.toggle = "" :
        this.dataset.toggle = "toggle";

    if (this.dataset.system) {
        var postfix = " стоит " + this.title;
    } else {
        var postfix = "";
    };
    dataField.innerHTML = this.dataset.range + postfix;
}

function untoggle() {
    toggles = document.querySelectorAll(".cell");
    toggles.forEach(function (value) {
        value.dataset.toggle = "";
    });
    dataField.innerHTML = "Ничего не выделено";
}

var dataDiv = document.createElement("div"),
    dataField = document.createElement("div"),
    dataActions = document.createElement("div"),
    dataActionClearSelection = document.createElement("button"),
    dataActionEatFigure = document.createElement("button");
dataField.addEventListener('input', function () {
    console.log("this.innerHTML");
})
dataActionClearSelection.innerHTML = "Очистить";
dataActionEatFigure.innerHTML = "Съесть/Вернуть";
dataActions.appendChild(dataActionClearSelection);
dataActions.appendChild(dataActionEatFigure);
dataDiv.setAttribute("class", "data-container");
dataActions.setAttribute("class", "clean-block data-field data-actions");
dataField.setAttribute("class", "clean-block data-field");
dataField.innerHTML = "Ничего не выделено";
dataActionClearSelection.addEventListener("click", untoggle);
dataDiv.appendChild(dataField);
dataDiv.appendChild(dataActions);
chessboard.appendChild(dataDiv);
window.onload = function () {
    toggles.forEach(function (value, item, array) {
        value.addEventListener("click", toggle);
    })
}

//////
var eatenField = document.createElement("div");
eatenField.setAttribute("id", "eaten-container");
chessboard.insertAdjacentElement("beforeend", eatenField);
dataActionEatFigure.addEventListener('click', eatIt);
// Съедалка и возвращалка из съедалки
function eatIt() {
    var eatFigure = document.querySelector("[data-toggle='toggle']");
    if (!eatFigure || !eatFigure.dataset.system) {
        dataField.innerHTML = "Пусто";
        return;
    }
    if (eatFigure.dataset.from !== undefined && eatFigure.dataset.system !== undefined) {
        var comeBack = document.getElementById(eatFigure.dataset.from);
        comeBack.innerHTML = figures[eatFigure.dataset.system.split(".")[0]][eatFigure.dataset.system.split(".")[1]]["fig"];
        comeBack.dataset.toggle = "";
        comeBack.dataset.system = figures[eatFigure.dataset.system.split(".")[0]][eatFigure.dataset.system.split(".")[1]]["system"];
        comeBack.title = figures[eatFigure.dataset.system.split(".")[0]][eatFigure.dataset.system.split(".")[1]]["name"];
        dataField.innerHTML = "Вернули " + comeBack.title + " на " + comeBack.dataset.range;
        return eatFigure.remove();

    } else {
        if (eatenField.childNodes.length > 17) {
            return dataField.innerHTML = "Больше есть пока нельзя";
        };
        if (eatFigure.parentElement.id == "eaten-container") {
            dataField.innerHTML = "Пусто";
            return;
        }
        var eatFieldItem = document.createElement("div");
        eatFieldItem.setAttribute("class", "cell white-block");
        eatFieldItem.setAttribute("data-toggle", "");
        eatFieldItem.onselectstart = function () {
            return false;
        };
        eatFieldItem.unselectable = "on";
        eatFieldItem.style.MozUserSelect = "none";
        eatFieldItem.style.KhtmlUserSelect = "none";
        eatFieldItem.style.webkitUserSelect = "none";
        eatFieldItem.setAttribute("data-range", eatFigure.dataset.range);
        eatFieldItem.setAttribute("data-system", eatFigure.dataset.system);
        eatFieldItem.setAttribute("title", eatFigure.title + " c " + eatFigure.dataset.range);
        eatFieldItem.innerHTML = figures[eatFigure.dataset.system.split(".")[0]][eatFigure.dataset.system.split(".")[1]]["fig"];
        eatFieldItem.setAttribute("data-from", eatFigure.id);
        eatFieldItem.addEventListener("click", function toggle() {
            toggles = document.querySelectorAll(".cell");
            toggles.forEach(function (value) {
                value.dataset.toggle = "";
            })
            this.dataset.toggle == true ? this.dataset.toggle = "" :
                this.dataset.toggle = "toggle";

            if (this.dataset.system) {
                var postfix = " стоял " + this.title.slice(0, -5);
            } else {
                var postfix = "";
            };
            dataField.innerHTML = this.dataset.range + postfix;
        });
        eatenField.appendChild(eatFieldItem);
        toggles = document.querySelectorAll(".cell");
        eatFigure.innerHTML = "";
        eatFigure.title = "";
        eatFigure.dataset.system = "";
    };
}
